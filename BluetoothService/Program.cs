﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CAALHP.SOA.ICE.ClientAdapters;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;


namespace BluetoothService
{
    internal class Program
    {
        private BluetoothClient _bluetooth = new BluetoothClient();
        private const string NONIN_DEVICE = "Nonin_Medical_Inc._680956";
        private static ServiceAdapter _adapter;

        private static void Main(string[] args)
        {
            const string endpoint = "localhost";
            var reqImp = new BluetoothServiceImplementation();
            _adapter = new ServiceAdapter(endpoint, reqImp);


            BluetoothClient _bluetooth = new BluetoothClient();
            const string NONIN_DEVICE = "Nonin_Medical_Inc._680956";

            Console.WriteLine("Searching for Bluetooth Device...");
            var gadgeteerDevice = _bluetooth.DiscoverDevices().FirstOrDefault(x => x.DeviceName == NONIN_DEVICE);
            bool run = false;


            while (!run)
            {
                try
                {
                    if (gadgeteerDevice != null)
                    {
                        _bluetooth.Connect(gadgeteerDevice.DeviceAddress,
                                           InTheHand.Net.Bluetooth.BluetoothService.SerialPort);
                        if (_bluetooth.Connected)
                        {
                            run = true;
                            Console.WriteLine("Bluetooth Device Found!");

                            //reqImp.SendOpenProgramRequest(); //try to open marioKol       

                           reqImp.SendOpenMarioKolEvent();
                        }
                    }
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e.ToString());
                    Thread.Sleep(1000);
                }
            }

            Console.ReadKey();
        }

        

    }
}
