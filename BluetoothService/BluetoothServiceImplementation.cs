﻿using System;
using System.Collections.Generic;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;

namespace BluetoothService
{
    class BluetoothServiceImplementation : CAALHP.Contracts.IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host { get; set; }

        private int _processId;
        private const string AppName = "BluetoothService";

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(AppName))
            {
                //Show homescreen
                //Show();
            }
        }

        public void SendOpenMarioKolEvent()
        {
            
            var request = new OpenMarioKolEvent();
            request.CallerName = AppName;

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, request, AppName);
            _host.Host.ReportEvent(serializedCommand);

            Console.WriteLine("Request for Mario start sent!");
        }

        public string GetName()
        {
            return AppName;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void SendOpenProgramRequest()
        {
            var showAppEvent = new ShowAppEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                AppName = "MarioKol"
            };
            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, showAppEvent);
            _host.Host.ReportEvent(serializedEvent);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            // Subscribe to ShowAppEvents
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
        }

        public void Start()
        {
            throw new NotImplementedException();

        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }

    }
}
